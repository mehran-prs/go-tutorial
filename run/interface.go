package main

import (
	"fmt"
)

type eye interface {
	EyeColor() string
}

type Animal interface {
	eye
	Name() string
}

type Dog struct {
	Color string
}

type WildDog struct {
	*Dog

	DogType string
}

func (d *Dog) EyeColor() string {
	return d.Color
}

func (d *Dog) Name() string {
	return "dog is this animal name"
}

func (d *WildDog) KilledHumansCount() int {
	return 43
}

func main() {
	var animal Animal

	// Do not forget to use "&" when assigning a type to interface.
	// this is because we are getting pointer when implementing
	// interface methods for Dog
	animal = &Dog{Color: "red"}

	fmt.Println(animal.Name())

	wildDog := WildDog{DogType: "d", Dog: &Dog{Color: "blue"}}

	fmt.Println(wildDog.KilledHumansCount())

	// Type assertion:(access to interface concrete value)

	myDog:= animal.(*Dog)

	println(myDog.Color)

	// Another example:
	var a interface{} = "ali"
	val, ok := a.(string)

	if ok {
		println(val)
	}
}
