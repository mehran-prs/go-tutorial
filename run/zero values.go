package main

func main() {
	// types that have "nil" as default values
	// slice,map,chan,pointers(e.g *int).

	// array zero value is an concrete array of that length.

	//e.g this is zero array with value of [0,0,0,0,0]
	//var a [5]int



	//---------------------------------------
	//  Make vs new
	//---------------------------------------
	// * "new" return pointer, but make return value.

	// * "new" is good to creating pointers to non-composite types.

	// * Use "make" to allocate memory to "slice,map,chan" (new
	// do not allocate memory, just create zero value
	// of each type, and zero value of map,slice and channel
	// is nil, so new create nil for this three types.)







}
