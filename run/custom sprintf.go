package main

import "fmt"

type Human struct {
	name string
}

func (h Human) String() string {
	return fmt.Sprintf("Hey, my name is %s", h.name)
}

func main() {
	fmt.Println(Human{name: "ali"})
}
