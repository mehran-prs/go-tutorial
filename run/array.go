package main

import "fmt"

func main() {

	//var a = map[string]string{}
	//var a = make(map[string]string)
	//var a = make([]int,5,10)
	//println(a)
	//fmt.Println(a)
	//var a = make([]int, 0, 10)
	//fmt.Println(len(a))
	//---------------------------------------
	//  Array
	//---------------------------------------

	// Define Array (fixed length):
	// Define length or use ... to golang detect length
	// by count of initial values.
	//a := [...]int{1, 2, 3, 4}
	//b := [4]int{1, 2, 3}
	//fmt.Println(b)
	//fmt.Println("a Array", a)
	//fmt.Println("b Array", b)
	//
	//fmt.Println("a for loop:")

	// loop over all values (even if do not set values,array
	// use zero value of that type for indexes).
	//for k, val := range b {
	//	fmt.Println(k, val)
	//}

	//---------------------------------------
	//  Slice
	//---------------------------------------
	//c := []int{1, 3, 4}
	//d := make([]int,0)
	//
	//d[0] = 4
	//d[1]
	//d=append(d,4)

	//fmt.Println("c slice: ", c)
	//fmt.Println("d slice: ", )

	//e := make([]int, 0, 3)
	//for i := 1; i < 6; i++ {
	//	e= append(e, i)
	//	fmt.Printf("cap %v, len %v, %p\n", cap(e), len(e), e)
	//	fmt.Println(e)
	//}

	f := []int{1, 2, 3, 4, 5}

	// Just set slice index pointer on array, do not craete
	// new array until length is ok
	g := f[:3] // [low index:high index(exclude self)]

	//g[2] = 10
	//g = append(g, 100)
	//g = append(g, 100)
	//g = append(g, 100)
	//fmt.Println(f)
	//fmt.Println(g)
	//fmt.Printf("array address: %p\n", f)
	//fmt.Printf("slice address: %p\n", g)
	//
	//fmt.Println(f, g)
	//fmt.Println("slice len:", len(g), "slice cap:", cap(g))
}
