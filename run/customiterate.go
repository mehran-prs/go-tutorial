package main

import "fmt"

func main() {
	iter := Iterator()
	fmt.Println(iter())
	fmt.Println(iter())
	fmt.Println(iter())
}

func Iterator() func() int {
	i := 0

	return func() int {
		i += 1
		return i
	}
}
