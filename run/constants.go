package main

import "fmt"

/*
	In golang constants(1,2.4,"val") are untyped and can assign to any compatible
	type (e.g 4 to int,int8,int16 variable)
	helper link:https://www.callicoder.com/golang-typed-untyped-constants/
	e.g fo untyped values:
1
12.6
"arch"
*/

const name = "reza"      // untyped constant
const typedConst int = 4 // typed constant
const untypedConst = 4

const (
	aVal int = 4       //typed constant
	bVal     = "check" // untyped constant
)

//const (
//	val1 = iota
//	val2 = iota + 2
//	val3
//)

//const (
//	_    = iota + 1
//	val2 = iota + 3
//	_ // skip next value
//	val3
//)

func main() {

	//typed variable (we assigned untyped value and
	// then compiler set default untyped value type to it)
	var val int16 = 4

	// Can not mix typed const wit variables
	//val+=typedConst

	// But can mix untyped const with variables
	val += untypedConst

	// valid:Because 12 and 12.5 are untyepd constants , so we can mix them.
	var a = 12 + 12.5

	fmt.Println(a)

	// Because variables are typed, so we can not mix typed values.
	//var b = 12
	//var c = 12.5
	// invalid
	//var d = b + c
	//fmt.Println(d)

}
