package main

import "fmt"

func main() {

	// Make create slice,map,channel (3 reference types)
	// Make return regular values, make does not return
	// pointer values.
	// Make function is run time because by golang syntax we can
	// not define function like make.
	mapVal := make(map[int]string)

	//mapVal := map[int]string{}

	// GoToRoute generate pointer to zero value of each type: so if
	// crate reference type with new, value will be nil (because
	// zero value of reference types is nil), so you can not
	// assign values to map that created by new ! (because it's nil)
	//mapVal := *new(map[int]string)

	mapVal[4] = "check"

	println("Map: ", mapVal[4])

	println("len: ", len(mapVal))

	delete(mapVal, 4)

	mapVal[0] = "val"
	mapVal[3] = "another value"

	val, exists := mapVal[4]

	println("Val: ", val)
	println("Exists: ", exists)

	fmt.Println(mapVal)

	commits := map[string]int{
		"first":  3423,
		"second": 342343,
		"third":  9844,
		"forth":  343223,
	}

	for key, val := range commits {
		fmt.Println(key, val)
	}

	//mm := map[string]map[string]string{
	//	"a": {
	//		"b": "c",
	//	},
	//}
}
