package main

/*
* Basic Types: int,string,rune,...

* Composite Types
Go supports the following composite types:
	pointer types - C pointer alike.

	struct types - C struct alike.

	function types - functions are first-class types in Go.

	container types:
		array types - fixed-length container types.
		slice type - dynamic-length and dynamic-capacity container types.
		map types - maps are associative arrays (or dictionaries). The standard Go compiler implements maps as hashtables.

	channel types - channels are used to synchronize data among goroutines (the green threads in Go).

	interface types - interfaces play a key role in reflection and polymorphism.

*/

//---------------------------------------
//  Named types
//---------------------------------------

// Type definition
type MyInt int

type Password string

// Type Alias
type AliasInt = int

type table = map[string]string

// Important Note: Assigning a value of one named type
// to variable of a different named type is forbidden,
// even if the underlying type is the same(must convert).
// Assignment between related named and unamed types is
// allowed though.
// See this link : https://stackoverflow.com/questions/32983546/named-and-unnamed-types

func main() {
	//var myIntVal MyInt = 1
	//var intVal = 5

	//var a = "ali"
	//var b Password=a

	//intVal = myIntVal

	// invalid (should convert)
	//myIntVal = intVal

	// valid
	//myIntVal = MyInt(intVal)

	//fmt.Println(myIntVal)
	//fmt.Println(intVal)

	//var aliasInt AliasInt = 4
	//var intVal = 5

	//aliasInt = intVal

	//fmt.Println(aliasInt)
	//fmt.Println(intVal)

	//tb := table{"first": "value"}
	//fmt.Println(tb)

	//---------------------------------------
	//  Example of assigning named and
	//  unnamed types to each other
	//---------------------------------------

	type A struct {
		name string
	}

	type B A

	//var a = A("ali")
	//var b = B("reza")
	var c = struct {
		name string
	}{"mehran"}
	//var aa A=b
	//invalid
	var a A = A{name: "ali"}
	var b B = B{name: "reza"}
	a=b
	//a=c

	// valid because we are assigning unnamed
	// type with same structure to named type
	//a=c

	// valid
	//c=b

}
