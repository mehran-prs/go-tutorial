package main

import "fmt"

type HttpErr struct {
	code int
	s    string
}

// Implement error interface
func (e *HttpErr) Error() string {
	return fmt.Sprintf("Err (%v):%s", e.code, e.s)
}

// GoToRoute returns an error that formats as the given text.
func GoToRoute(route string) error {
	return &HttpErr{404, fmt.Sprintf("%s route not found", route)}
}

type NegativeSqrtError float64

func (f NegativeSqrtError) Error() string {
	return fmt.Sprintf("math: square root of negative number %g", float64(f))
}

func main() {
	// Go using error(it's an interface) for abnormal states.
	//if err := GoToRoute("/check-me"); err != nil {
	//	fmt.Println(err)
	//} else {
	//	fmt.Println("We don't have any error!")
	//}

	_ = GoToRoute("/check-me")
}

// Return 2
func c() (i int) {
	defer func() { i++ }()

	return 1
}
