package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"sync"
	"sync/atomic"
	"time"
)

func main() {
	//base()
	//channel()
	//channelBuffering()
	//channelSynchronization()
	//channelDirection()
	//selectCase()
	//iterateChannels()
	//pool()
	//waitGroup()
	//tick()
	//rateLimitByTime()
	//atomicCounter()
	//mutex()
	//SimulateMutex()
	parallelLoop()
}

//---------------------------------------
//  Base
//---------------------------------------
func base() {
	go func(msg string) {
		fmt.Println(msg)
	}("Hello")

	fmt.Println("saying...")
	time.Sleep(1 * time.Millisecond)
}

//---------------------------------------
//  channels
//---------------------------------------
func channel() {
	var simpleChannel = make(chan string)

	var check = func() {
		msg := <-simpleChannel
		fmt.Println("msg is: ", msg)
	}

	go check()

	simpleChannel <- "Hello"

	time.Sleep(1 * time.Millisecond)
}

func channelBuffering() {
	var simpleChannel = make(chan string, 1)

	var check = func() {
		msg := <-simpleChannel
		fmt.Println("msg is: ", msg)
	}

	go check()

	simpleChannel <- "Hello"

	// If channel don't have a any buffer,
	// we block here until a goroutine
	// pull this value from channel.
	simpleChannel <- "Hello"

	time.Sleep(1 * time.Millisecond)
}

func channelSynchronization() {
	var doSomething = func(done chan bool) {
		fmt.Println("Every thing is good and all jobs is done!")
		done <- true
	}

	done := make(chan bool)

	go doSomething(done)

	<-done
	fmt.Println("Done!")
}

func channelDirection() {
	ch := make(chan string)

	// input
	go func(myChannel chan<- string) {
		myChannel <- "Hey hello"
	}(ch)

	// output
	go func(out <-chan string) {
		res := <-out
		fmt.Println(res)
	}(ch)

	time.Sleep(1 * time.Millisecond)
}

func selectCase() {
	ch1 := make(chan string)
	ch2 := make(chan string)

	go func() {
		time.Sleep(500 * time.Millisecond)
		ch1 <- "Hey, first goroutine's msg"
	}()

	go func() {
		time.Sleep(1 * time.Second)
		ch2 <- "Hey, second goroutine's msg"
	}()

	for i := 0; i < 2; i++ {
		select {
		case msg := <-ch1:
			fmt.Println(msg)
		case msg := <-ch2:
			fmt.Println(msg)
		case <-time.After(1 * time.Millisecond):
			fmt.Println("Time is out, bye!")
		// Immediately get the value if do not
		// have any value on any channel for now
		default:
			fmt.Println("Default block!")
		}
	}

}

func iterateChannels() {

	// Close channels:
	list := make(chan int)
	done := make(chan bool)

	go func() {
		for {
			code, isOpen := <-list

			if isOpen {
				fmt.Printf("received code %d\n", code)
			} else {
				fmt.Println("bye")
				done <- true
				return
			}
		}

		// in for range of channel can not get second param,
		// loop break after closing channel
		//for code := range list {
		//	fmt.Printf("received code %d\n", code)
		//}

		//fmt.Println("bye")
		//done <- true
		//return

	}()

	list <- 1
	//var data,isOpen = <-list
	list <- 2

	close(list)
	<-done
}

func pool() {
	// To create a pool , just use go inside a loop:
	var decodeWorker = func(jobs <-chan string, res chan<- string) {
		for job := range jobs {
			fmt.Println("decoding ", job)
			time.Sleep(10 * time.Millisecond)
			fmt.Println("Decoding ", job, " finished.")
			res <- job + " Decoded"
		}
	}

	jobs, res := make(chan string, 15), make(chan string, 15)

	var workerCount = 4
	for i := 0; i < workerCount; i++ {
		go decodeWorker(jobs, res)
	}

	// populate jobs:
	for i := 0; i < 15; i++ {
		jobs <- "Job " + strconv.Itoa(i)
	}

	// If you do not closing jobs, so
	close(jobs)

	for i := 0; i < 15; i++ {
		fmt.Println(<-res)
	}

	fmt.Println("done!")
}

func waitGroup() {
	var wg sync.WaitGroup

	var worker = func(id int, wg *sync.WaitGroup) {
		fmt.Println("worker ", id)
		time.Sleep(100 * time.Millisecond)
		wg.Done()
	}

	for i := 1; i <= 4; i++ {
		wg.Add(1)
		go worker(i, &wg)
	}

	wg.Wait()

	fmt.Println("All of the workers are done!")
}

func tick() {
	// Timer:
	var timer = time.NewTimer(2 * time.Second)

	<-timer.C
	fmt.Println("After 2 second")

	var timer2 = time.NewTimer(2 * time.Second)

	timer2.Stop()
	fmt.Println("Stopped!")

	// time ticker
	ticker := time.NewTicker(2 * time.Second)
	for t := range ticker.C {
		fmt.Println(t)
	}
}

func rateLimitByTime() {
	//requests := make(chan int, 5)
	//for i := 1; i <= 5; i++ {
	//	requests <- i
	//}
	//close(requests)
	//
	//limiter := time.Tick(1 * time.Second)
	//
	//for req := range requests {
	//	<-limiter
	//	fmt.Println("request", req, time.Now())
	//}

	//---------------------------------------
	//  Burst Limit
	//---------------------------------------
	burstyLimiter := make(chan time.Time, 3)

	for i := 0; i < 3; i++ {
		burstyLimiter <- time.Now()
	}

	go func() {
		//Tick is shortcut for NewTicker().C
		for t := range time.Tick(time.Second) {
			burstyLimiter <- t
		}
	}()

	burstyRequests := make(chan int, 5)
	for i := 1; i <= 5; i++ {
		burstyRequests <- i
	}
	close(burstyRequests)

	for req := range burstyRequests {
		<-burstyLimiter
		fmt.Println("request", req, time.Now())
	}
}

func atomicCounter() {
	var val uint32

	var wg sync.WaitGroup

	for i := 0; i < 1000; i++ {
		wg.Add(1)
		go func() {
			atomic.AddUint32(&val, 10)
			//val += 10 // wrong , run with -race flag to see data race
			wg.Done()
		}()
	}
	wg.Wait()
	fmt.Println("val:", val)
}

func mutex() {
	var state = make(map[int]int)

	var mutex = &sync.Mutex{}

	var readOps uint64
	var writeOps uint64

	for r := 0; r < 100; r++ {
		go func() {
			total := 0
			for {

				key := rand.Intn(5)

				// Critical Section : Use mutex to prevent race condition.
				// Note: If one Goroutine already holds the lock and if a new
				// Goroutine is trying to acquire a lock, the new
				// Goroutine will be blocked until the mutex is
				// unlocked.
				mutex.Lock()
				total += state[key]
				mutex.Unlock()
				atomic.AddUint64(&readOps, 1)
				time.Sleep(time.Millisecond)
			}
		}()
	}

	for w := 0; w < 10; w++ {
		go func() {
			for {
				key := rand.Intn(5)
				val := rand.Intn(100)
				// Critical Section
				mutex.Lock()
				state[key] = val
				mutex.Unlock()
				atomic.AddUint64(&writeOps, 1)
				time.Sleep(time.Millisecond)
			}
		}()
	}

	time.Sleep(time.Second)

	readOpsFinal := atomic.LoadUint64(&readOps)
	fmt.Println("readOps:", readOpsFinal)
	writeOpsFinal := atomic.LoadUint64(&writeOps)
	fmt.Println("writeOps:", writeOpsFinal)

	mutex.Lock()
	fmt.Println("state:", state)
	mutex.Unlock()
}

func SimulateMutex() {

	// In general use channels when Goroutines need to communicate
	// with each other and mutexes when only one Goroutine should
	// access the critical section of code.

	var val uint16
	var wg sync.WaitGroup

	var isBussy = make(chan bool, 1)

	for i := 0; i < 1000; i++ {
		wg.Add(1)

		go func() {
			isBussy <- true
			val += 10
			<-isBussy
			wg.Done()
		}()
	}

	wg.Wait()

	// Because all of goroutines are done, don't
	// need to atomic read for "val" variable.
	fmt.Println("Val is:", val)
}

func parallelLoop() {

	values := [...]string{"test", "is", "ok"}

	for _, val := range values {
		go func(val *string) {
			time.Sleep(time.Second)
			fmt.Println(*val)
		}(&val)
	}

	time.Sleep(2 * time.Second)
}
