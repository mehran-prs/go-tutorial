package main

import "fmt"

func main() {
	//fmt.Println("b is:", b())

	f()
	fmt.Println("Returned normally from f.")

	//Real Usage: mutex,...
	//mu.Lock()
	//defer mu.Unlock()
}

// in defer functions we can chang named
// returned values.
//func b() (i int) {
//	defer func() { i++ }()
//
//	return 4
//}

func f() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in f", r)
		}
	}()

	fmt.Println("Calling g.")
	g(0)
	fmt.Println("Returned normally from g.")
}

func g(i int) {
	if i > 3 {
		fmt.Println("Panicking!")
		panic(fmt.Sprintf("%v", i))
	}

	defer fmt.Println("Defer in g", i)

	fmt.Println("Printing in g", i)

	g(i + 1)
}
